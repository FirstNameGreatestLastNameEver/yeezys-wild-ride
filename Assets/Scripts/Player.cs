﻿using UnityEngine;
using System.Collections;
public class Player : MonoBehaviour {

	//Variables
	public float speed = 6.0F;
	public float jumpSpeed = 8.0F; 
	public float gravity = 20.0F;
	private Vector3 moveDirection = Vector3.zero;
	private Animator animator;
	private BoxCollider boxCollider;
	
	void Update() {
		boxCollider = GetComponent<BoxCollider>();
		CharacterController controller = GetComponent<CharacterController>();

		animator = GetComponent<Animator>();
		// is the controller on the ground?
		if (controller.isGrounded) {
			//Feed moveDirection with input.
			moveDirection = new Vector3(Input.GetAxis("Horizontal"), 0, Input.GetAxis("Vertical"));

			moveDirection = transform.TransformDirection(moveDirection);

			//Multiply it by speed.
			moveDirection *= speed;
			if( Input.GetKey("right") )
			{
				animator.SetTrigger("YeezyMove");
			}if( Input.GetKey("left") )
			{
				animator.SetTrigger("YeezyMove2");
			}

			//Jumping
			if (Input.GetButton("Jump")){

				moveDirection.y = jumpSpeed;
			}
			
		}

		//Applying gravity to the controller
		moveDirection.y -= gravity * Time.deltaTime;
		//Making the character move
		controller.Move(moveDirection * Time.deltaTime);
	}
	private void OnTriggerEnter(Collider objectPlayerCollidedWith){
		if (objectPlayerCollidedWith.tag == "Coin") {
			objectPlayerCollidedWith.gameObject.SetActive(false);
		}

}
}
